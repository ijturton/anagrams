package com.ianturton.utilities;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.geotools.data.DataUtilities;
import org.geotools.feature.SchemaException;
import org.junit.Test;
import org.opengis.feature.simple.SimpleFeature;
import org.opengis.feature.simple.SimpleFeatureType;

public class WriteShapefileTest {

  @Test
  public void testWriteFeatures() throws SchemaException, IOException {
    SimpleFeatureType schema = DataUtilities.createType("test", "centroid:Point");
    List<SimpleFeature> pointFeatures = new ArrayList<>();
    for(int i=0;i<10;i++) {
      pointFeatures.add(GenerateRandomData.createSimplePointFeature(schema));
    }
    File f = File.createTempFile("writer-test", ".shp");
    WriteShapefile writer = new WriteShapefile(f);
    writer.writeFeatures(DataUtilities.collection(pointFeatures));
    assertTrue(f.exists());
  }

}
