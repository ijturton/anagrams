package com.parthparekh.algorithms;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;

import org.junit.Before;
import org.junit.Test;


/**
 * Unit test for SortedWordDictionary class
 *
 * @author: Parth Parekh
 */
public class SortedWordDictionaryTest {

    private SortedWordDictionary sortedWordDictionary;

    @Before
    public void setUp() throws IOException {
        sortedWordDictionary = new SortedWordDictionary();
        loadDictionary();
    }

    private void loadDictionary() throws IOException {
        String executionPath = System.getProperty("user.dir");
        String fileSeparator = System.getProperty("file.separator");
        sortedWordDictionary.loadDictionary(executionPath + fileSeparator + "wordlist" + fileSeparator + "wordlist.txt");
    }

    @Test
    public void fileLoadTest() {
        assertTrue(sortedWordDictionary.isDictionaryLoaded());
    }

    @Test
    public void addWordTest() {
        assertTrue(sortedWordDictionary.addWord("parth"));
    }

    @Test
    public void findSingleWordAnagramsTest() {
        assertEquals(4, sortedWordDictionary.findSingleWordAnagrams("tea").size());
        assertEquals(5, sortedWordDictionary.findSingleWordAnagrams("enlist").size());
    }
}
