package com.ianturton.anagrams;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.geotools.data.DataStore;
import org.geotools.data.DataStoreFinder;
import org.geotools.data.DataUtilities;
import org.geotools.data.postgis.PostgisNGDataStoreFactory;
import org.geotools.data.simple.SimpleFeatureCollection;
import org.geotools.data.simple.SimpleFeatureIterator;
import org.geotools.data.simple.SimpleFeatureSource;
import org.geotools.feature.NameImpl;
import org.geotools.feature.simple.SimpleFeatureBuilder;
import org.geotools.feature.simple.SimpleFeatureTypeBuilder;
import org.geotools.feature.type.GeometryDescriptorImpl;
import org.geotools.filter.text.cql2.CQL;
import org.geotools.filter.text.cql2.CQLException;
import org.opengis.feature.simple.SimpleFeature;
import org.opengis.feature.simple.SimpleFeatureType;
import org.opengis.feature.type.AttributeDescriptor;
import org.opengis.feature.type.AttributeType;
import org.opengis.feature.type.GeometryDescriptor;
import org.opengis.feature.type.GeometryType;
import org.opengis.filter.identity.FeatureId;

import com.google.common.collect.HashMultimap;
import com.ianturton.utilities.WriteShapefile;
import com.parthparekh.algorithms.AnagramSolver;

public class AnagramLookup {
  private Map<String, Collection<String>> dict;
  private AnagramSolver solver;

  public AnagramLookup() throws FileNotFoundException, IOException {
    // change this to point to your dictionary (one word per line)
    // File f = new File("/usr/share/dict/british-english");
    File f = new File("/home/ian/Downloads/scowl-british.txt");

    HashMultimap<String, String> indexedDictionary = HashMultimap.create();
    try (BufferedReader buf = new BufferedReader(new FileReader(f))) {
      String line;
      // read each word in the dictionary
      while ((line = buf.readLine()) != null) {
        // strip out non letters
        String word = line.toLowerCase().replaceAll("\\W", "");
        // store the word against the sorted key
        indexedDictionary.put(sort(word), word);
      }
    }
    dict = indexedDictionary.asMap();
    solver = new AnagramSolver(2, f.getAbsolutePath());
  }

  public Collection<String> getAnagrams(String word) {
    return dict.get(sort(word));
  }

  /**
   * Sort the characters of a word alphabetically
   * @param word - string to sort
   * @return sorted string
   */
  private String sort(String word) {
    char[] charArray = word.toLowerCase().toCharArray();
    Arrays.sort(charArray);
    return new String(charArray);
  }

  private String current;
  private HashMap<String, Set<String>> results = new HashMap<>();
  private HashMap<String, Set<String>> done = new HashMap<>();
  private void process(Map<String, Object> params) throws IOException, CQLException {
    DataStore ds = DataStoreFinder.getDataStore(params);
    if (ds == null) {
      throw new RuntimeException("No datastore");
    }

    SimpleFeatureSource fs = ds.getFeatureSource(tableName);

    SimpleFeatureCollection features = fs.getFeatures(CQL.toFilter(cqlFilter));
    ArrayList<SimpleFeature> anagramFeatures = new ArrayList<>();
    SimpleFeatureType nSchema = addColumn(fs.getSchema(), "anagram", String.class);
    SimpleFeatureType newSchema = addColumn(nSchema, "anagrams", String.class);
    try (SimpleFeatureIterator itr = features.features()) {
      int count = 0;
      while (itr.hasNext() /*&& count++ < 100*/) {

        SimpleFeature f = itr.next();

        String name = (String) f.getAttribute(columnName);

        current = name.trim().toLowerCase().replaceAll("\\W", "");
        //if we have looked at this name (with or without spaces etc) skip it as we know the answer
        if(done.containsKey(sort(current))) {
          results.put(name, done.get(sort(current)));
          continue;
        }
        Collection<String> anagrams = getAnagrams(current);

        if (anagrams != null && !anagrams.isEmpty()) {
          // remove the name itself if it happens to be a word

          if (!anagrams.isEmpty()) {
            TreeSet<String> res = new TreeSet<String>(anagrams);

            Set<Set<String>> anagramSet = solver.findAllAnagrams(current);
            if (anagramSet != null && !anagramSet.isEmpty()) {
              for (Set<String> s : anagramSet) {
                if (s != null) {
                  String potential = s.toString().replace(",", "").replace("[", "").replace("]", "");
                  if(!current.equals(potential.replaceAll("\\W", ""))) {
                    res.add(potential);
                  }
                }
              }
            }
            res.remove(current);
            if (!res.isEmpty()) {
              results.put(name, res);
              done.put(sort(current), res);
              Iterator<String> iterator = res.iterator();
              String first = null, all;
              StringBuilder builder = new StringBuilder();
              while (iterator.hasNext()) {
                String next = iterator.next();
                if (first == null) {
                  first = next;
                }
                builder.append(next).append(",");
              }
              SimpleFeature ana = addAttribute(f, nSchema, "anagram", first);
              all = builder.substring(0, builder.length() - 1);
              SimpleFeature ana1 = addAttribute(ana, newSchema, "anagrams", all);
              anagramFeatures.add(ana1);
            }
          }
        }
      }
    }
    // sort by length
    Set<String> keySet = new TreeSet<>(new Comparator<String>() {
      @Override
      public int compare(String s1, String s2) {
        s1 = s1.replaceAll("\\W", "");
        s2 = s2.replaceAll("\\W", "");
        if (s1.length() > s2.length()) {
          return -1;
        } else if (s1.length() < s2.length()) {
          return 1;
        } else {
          return s1.compareTo(s2);
        }
      }
    });
    File file = new File("gbanagrams.shp");

    WriteShapefile writer = new WriteShapefile(file);
    writer.writeFeatures(DataUtilities.collection(anagramFeatures));
    

    keySet.addAll(results.keySet());
    try (FileWriter fWriter = new FileWriter(new File("gbanagrams.txt"))) {
      BufferedWriter bWriter = new BufferedWriter(fWriter);
      for (String k : keySet) {
        bWriter.write(k.replaceAll("\\W", "").length() + ":" + k + "\n");
        for (String s : results.get(k)) {
          bWriter.write("\t" + s + "\n");
        }
      }
      bWriter.close();
    }
  }

  private SimpleFeatureType addColumn(SimpleFeatureType schema, String name, Class type) {

    SimpleFeatureTypeBuilder builder = new SimpleFeatureTypeBuilder();
    builder.setName(schema.getName());
    builder.setName(schema.getName());

    builder.setSuperType((SimpleFeatureType) schema.getSuper());
    for (AttributeDescriptor att : schema.getAttributeDescriptors()) {
      AttributeType aType = att.getType();
      if (aType instanceof GeometryType) {
        GeometryType geomType = (GeometryType) aType;
        GeometryDescriptor geom = (GeometryDescriptor) att;

        GeometryDescriptor geomDesc = new GeometryDescriptorImpl(geomType, new NameImpl(geom.getLocalName()),
            geom.getMinOccurs(), geom.getMaxOccurs(), geom.isNillable(), geom.getDefaultValue());
        builder.add(geomDesc);
        builder.setDefaultGeometry(geom.getLocalName());
      } else {
        builder.add(att);
      }
    }
    builder.add(name, type);
    return builder.buildFeatureType();
  }

  private SimpleFeature addAttribute(SimpleFeature source, SimpleFeatureType schema, String name, Object val) {
    // SimpleFeatureType schema = source.getFeatureType();
    SimpleFeatureBuilder builder = new SimpleFeatureBuilder(schema);
    for (int i = 0; i < schema.getAttributeCount(); i++) {
      AttributeDescriptor attributeType = schema.getDescriptor(i);
      Object value = null;
      String n = attributeType.getLocalName();
      if (schema.getDescriptor(n) != null) {
        value = source.getAttribute(n);
      }

      builder.set(n, value);
    }
    builder.set(name, val);
    FeatureId id = source.getIdentifier();
    SimpleFeature retyped = builder.buildFeature(id.getID());
    retyped.getUserData().putAll(source.getUserData());
    return retyped;
  }

  static String schemaName = "opennames";// "geonames";
  String tableName = "opennames";// "geoname";
  String columnName = "name1";// "asciiname";
  String cqlFilter = "type = 'populatedPlace'";// "not asciiname is null";

  public static void main(String[] args) throws IOException, CQLException {
    Map<String, Object> params = new HashMap<String, Object>();
    params.put(PostgisNGDataStoreFactory.DBTYPE.key, PostgisNGDataStoreFactory.DBTYPE.sample);
    params.put(PostgisNGDataStoreFactory.USER.key, "ian");
    params.put(PostgisNGDataStoreFactory.PASSWD.key, "ianian");

    params.put(PostgisNGDataStoreFactory.SCHEMA.key, schemaName);
    params.put(PostgisNGDataStoreFactory.DATABASE.key, "osdata");
    params.put(PostgisNGDataStoreFactory.HOST.key, "127.0.0.1");
    params.put(PostgisNGDataStoreFactory.PORT.key, "5432");

    AnagramLookup me = new AnagramLookup();
    me.process(params);
  }
}
