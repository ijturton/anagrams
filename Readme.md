Anagrams for Place Names
========================

Code to generate anagrams of place names.

You will need to add a database of placenames (and locations) and set up the
parameters to open it in `main`. You will also need to specify the table, schema
and column name and a CQL filter to pull out the names that you are interested
in.

Word Lists
==========

You need a list of words (in the language you would like your anagrams in) one
per line.

Initial experiments with the Unix system dictonary worked quite well
until the possibility to use short words in anagram phrases was added,
then some very odd short words showed up. I have had good results
using the [SCOWL lists](http://wordlist.aspell.net/scowl-readme/)
at 55 or 60, but your tolerance for odd words may
be higher than mine. Another possibility is the [EOWL
files](http://dreamsteep.com/projects/the-english-open-word-list.html)
but again I found it had too many odd words.


Installation
------------

mvn install

mvn exec:java -Dexec.mainClass="com.ianturton.anagrams.AnagramLookup"

This will produce an list of placenames and anagrams in `gbanagram.txt` and in a
shapefile call `gbanagram.shp`.

Licence
-------

MIT Licence, includes modified code from
https://github.com/parekhparth/AnagramSolver
