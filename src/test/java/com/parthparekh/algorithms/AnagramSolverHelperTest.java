package com.parthparekh.algorithms;


import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.junit.Test;


/**
 * Unit test for AnagramSolverHelper functions
 *
 * @author: Parth Parekh
 */
public class AnagramSolverHelperTest {

  @Test
  public void isSubsetTest() {
    assertTrue(AnagramSolverHelper.isSubset("parth".toCharArray(), "partah".toCharArray()));
    assertFalse(AnagramSolverHelper.isSubset("partha".toCharArray(), "parth".toCharArray()));
    assertTrue(AnagramSolverHelper.isSubset("parth".toCharArray(), "parth".toCharArray()));
    assertTrue(AnagramSolverHelper.isSubset("part".toCharArray(), "parth".toCharArray()));
  }

  @Test
  public void isEquivalentTest() {
    assertTrue(AnagramSolverHelper.isEquivalent("parth".toCharArray(), "htrap".toCharArray()));
    assertFalse(AnagramSolverHelper.isEquivalent("parth".toCharArray(), "parekh".toCharArray()));
    assertFalse(AnagramSolverHelper.isEquivalent("part".toCharArray(), "parth".toCharArray()));
    assertTrue(AnagramSolverHelper.isEquivalent("parth".toCharArray(), "parth".toCharArray()));
  }

  @Test
  public void setDifferenceTest() {
    assertTrue(Arrays.equals("t".toCharArray(),
        AnagramSolverHelper.setDifference("parth".toCharArray(), "parekh".toCharArray())));
    assertTrue(Arrays.equals("".toCharArray(),
        AnagramSolverHelper.setDifference("parth".toCharArray(), "htrap".toCharArray())));
    assertTrue(Arrays.equals("parth".toCharArray(),
        AnagramSolverHelper.setDifference("parth".toCharArray(), "xyz".toCharArray())));
  }

  @Test
  public void setMultiplicationTest() {
    Set<String> set1 = new HashSet<String>();
    set1.add("parth");
    set1.add("parekh");

    Set<String> set2 = new HashSet<String>();
    set2.add("1");
    set2.add("2");

    Set<String> set3 = new HashSet<String>();
    set3.add("a");

    assertEquals(4, AnagramSolverHelper.setMultiplication(set1, set2, set3).size());
    set3.add("b");
    assertEquals(8, AnagramSolverHelper.setMultiplication(set1, set2, set3).size());
    set2.add("3");
    assertEquals(12, AnagramSolverHelper.setMultiplication(set1, set2, set3).size());
    set3.add("c");
    assertEquals(18, AnagramSolverHelper.setMultiplication(set1, set2, set3).size());
  }
}
